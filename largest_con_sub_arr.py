import random
def generate_random_list():
    return [random.randint(-5, 5) for i in range(random.randint(5, 10))]

# arr = [-2, -3, 4, -1, -2, 1, 5, -3]
arr = generate_random_list()
print(arr)

max_sum = 0
max_arr = []
sum_till_now = 0
temp_arr = []

for i in arr:
    sum_till_now += i
    temp_arr.append(i)
    if sum_till_now < 0:
        sum_till_now = 0
        temp_arr = []
    if max_sum < sum_till_now:
        max_arr = temp_arr[:]
        max_sum = sum_till_now
    print(max_sum, sum_till_now)

print(max_sum, max_arr)
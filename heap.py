import math
import random
def get_parent(index):
    return math.ceil((index - 2) / 2)

def get_childs(index):
    return [2 * index + 1, 2 * index + 2]

def bubble_up(heap, index, mode):
    if index <= 0:
        return
    parent = get_parent(index)
    if heap[index] < heap[parent] and mode == 'max':
        return
    if heap[index] > heap[parent] and mode == 'min':
        return
    heap[index], heap[parent] = heap[parent], heap[index]
    return bubble_up(heap, parent, mode)

def bubble_down(heap, index, mode):
    childs = get_childs(index)
    child_values = {}
    for child in childs:
        if child < len(heap):
            child_values[heap[child]] = child
    if not child_values:
        return
    compare = eval(mode)(child_values)
    child = child_values[compare]
    if heap[index] > compare and mode == 'max':
        return
    if heap[index] < compare and mode == 'min':
        return
    heap[index], heap[child] = compare, heap[index]
    return bubble_down(heap, child, mode)
    

def pop_heap(heap, mode):
    print(heap[0])
    if len(heap) < 2:
        return
    heap[0] = heap.pop()
    bubble_down(heap, 0, mode)
    pop_heap(heap, mode)
    

def main():
    heap = []
    # mode = input("Enter heap mode(min or max) >> ")
    mode = random.choice(['min', 'max'])
    if mode != 'max' and mode != 'min':
        raise Exception("invalid mode error: Enter either min or max")
    for i in range(random.randint(0, 10)):
        data = random.randint(0, 100)
        heap.append(data)
        bubble_up(heap, len(heap) - 1, mode)
    print(heap)
    pop_heap(heap, mode)
    
main()


import random
import math

def generate_random_list():
    return sorted([random.randint(0, 100) for i in range(random.randint(0, 10))])

def binary_search(arr, val):
    if not arr:
        return
    mid = math.floor(len(arr) / 2)
    print("Arr:", arr, "mid:", mid, "dat:", arr[mid])
    if arr[mid] < val:
        return binary_search(arr[mid+1:], val)
    elif arr[mid] > val:
        return binary_search(arr[:mid], val)
    else:
        return mid

def main():
    arr = generate_random_list()
    print(arr)
    while True:
        val = int(input('Enter a number to search: '))
        if binary_search(arr, val) != None:
            print("Found in array")
        else:
            print("Not present in the array")


main()